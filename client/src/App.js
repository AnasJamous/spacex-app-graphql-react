import React from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom'
import "./App.css";

// components Import
import Launch from "./components/Launch";
import Launches from "./components/Launches";

// Apollo Imports
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";

// import Logo
import spacex_logo from "./spacex_logo.jpg";

// Create Apollo Client
const client = new ApolloClient({
  uri: "http://localhost:5000/graphql",
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Router>
        <div className="container">
          <img
            src={spacex_logo}
            alt="spaceX"
            style={{ width: 300, display: "block", margin: "auto" }}
          />
          <Route exact path="/" component={Launches} />
          <Route exact path="/launch/:flight_number" component={Launch} />
        </div>
      </Router>
    </ApolloProvider>
  );
}

export default App;
